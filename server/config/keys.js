// find out which env the node execute in
if(process.env.NODE_ENV === 'production'){
    // the production envirenment  
    module.exports = require('./prod');
}else{
    // the developement envirenment 
    module.exports = require('./dev');
}